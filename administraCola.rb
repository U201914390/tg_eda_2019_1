def colaVacia
	$first= -1    # $ DOlar indica variable global
	$last = -1
end

def insertaValor(cola,indMax, valor)
	if $last < indMax
		$last = $last +1
		cola[$last] = valor
		if $first == -1
			$first=0
		end
	end
	return cola
end


def eliminaElemento (cola)
	if $first > -1
		valor = cola[$first]
		$first = $first +1 
		if $first > $last
			colaVacia
		end
	end
	return cola
end

def muestraCola (cola)
	cadena = ""
	if $first > -1
		for i in $first..$last
			cadena = cadena + cola[i].to_s + " "
		end
	end
	return cadena
end


cola = []
indMax = 4  # Tamaño que se establece para la cola
valor=10
colaVacia
cola=insertaValor(cola,indMax,valor)
print muestraCola(cola)
puts
valor=20
cola=insertaValor(cola,indMax,valor)
print muestraCola(cola)

puts
cola = eliminaElemento(cola)
print muestraCola(cola)